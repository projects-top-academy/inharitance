﻿#include <iostream>
#include <string>

class Employee {
protected:
    std::string name;
    std::string position;
    int salary;

public:
    Employee(const std::string& name, const std::string& position, int salary) : name(name), position(position), salary(salary) {}

    void displayInfo() const {
        std::cout << name << ", " << position << ", " << salary << "\n";
    }
};

class Programmer : public Employee
{
public:
    Programmer(const std::string& name): Employee(name, "programmer", 120000) {}

};

class Designer : public Employee
{
public:
    Designer(const std::string& name) : Employee(name, "designer", 115000) {}

};

class Boss : public Employee
{
public:
    Boss(const std::string& name) : Employee(name, "Boss", 115000) {}

};


int main()
{
    Programmer vasya("Vasya");
    Programmer lesha("Lesha");
    Programmer kolya("Kolya");

    Designer tanya("Tanya");
    Designer masha("Masha");

    Boss vadim("Vadim Vadimovich");

    vasya.displayInfo();
    lesha.displayInfo();
    kolya.displayInfo();
    tanya.displayInfo();
    masha.displayInfo();
    vadim.displayInfo();

    return 0;
}
